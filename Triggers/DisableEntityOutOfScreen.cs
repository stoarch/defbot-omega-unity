﻿using UnityEngine;
using System.Collections;

public class DisableEntityOutOfScreen : MonoBehaviour {

	void Update () 
	{
		var camPos = Camera.main.WorldToScreenPoint( transform.position );
		
		if( !Camera.main.pixelRect.Contains( camPos ) )
			gameObject.SetActive( false );	
	}
}
