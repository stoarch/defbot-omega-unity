﻿using UnityEngine;
using System.Collections;

public class SpawnEntityOnDestroy : MonoBehaviour {

	public GameObject entityPrefab;
	
	void OnDestroy()
	{
		var newEntity = Instantiate( entityPrefab, transform.position, Quaternion.identity ) as GameObject;
		 
		newEntity.SetActive( true );	
	}
}
