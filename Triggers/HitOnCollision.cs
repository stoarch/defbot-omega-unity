﻿using UnityEngine;
using System.Collections;

public class HitOnCollision : MonoBehaviour {

	public int damage = 1;
	public string enemyTag;
	
	void OnTriggerEnter2D( Collider2D other )
	{
		if( !other.gameObject.CompareTag( enemyTag ) )
			return;
			
		var life = other.gameObject.GetComponent<LifeMeter>();
		life.Hit( damage );
	}
}
