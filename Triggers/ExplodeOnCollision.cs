﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class ExplodeOnCollision : MonoBehaviour 
{
	public string explosionTag;
	public string enemyTag;
	
	public AudioClip explosionAudio;
	public AudioSource explosionAudioSource;

	private EntityPool pool;
	
	void Start()
	{
		if( !GameState.HasEntityPool() )
		{
			Debug.LogError( "Unable to find entity pool in game state" );
			gameObject.SetActive( false );
		}
		
		pool = GameState.GetEntityPool();
				
		if( !explosionAudioSource )
			explosionAudioSource = gameObject.GetComponent<AudioSource>();		
	}
	
	void OnTriggerEnter2D( Collider2D other )
	{	
		if( !other.gameObject.CompareTag( enemyTag ) )
			return;

		if( explosionTag != "" )
		{
			var explosion = pool.GetEntity( explosionTag );
			explosion.transform.position = transform.position;
			explosion.SetActive( true );
		}

		if( explosionAudio )
			GameScene.PlayOnce( explosionAudio );
			
		Deactivate();
	}
	
	void Deactivate()
	{
		gameObject.SetActive( false ); //all objects in pool, so we only need to deactivate self
	}	
}
