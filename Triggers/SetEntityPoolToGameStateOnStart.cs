﻿using UnityEngine;
using System.Collections;

public class SetEntityPoolToGameStateOnStart : MonoBehaviour {

	public EntityPool pool;
	
	void Start () {
		GameState.SetEntityPool( pool );	
	}
}
