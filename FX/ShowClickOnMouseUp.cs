﻿using UnityEngine;
using System.Collections;

public class ShowClickOnMouseUp : MonoBehaviour {

	public BoxCollider2D bounds;
	
	void Update () {
	
		if( Input.GetMouseButtonUp(0) )
		{
			var coord = Camera.main.ScreenToWorldPoint( Input.mousePosition );
			
			if( bounds.OverlapPoint( coord ) )
			{
				coord.z = -1;
				transform.position = coord;
				
				var sprite = GetComponent<SpriteRenderer>();
				
				sprite.enabled = true;
				
				var animator =  GetComponent<Animator>();
				
				animator.SetTrigger("Click");
			}
		}	
	}
}
