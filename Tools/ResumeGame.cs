﻿using UnityEngine;
using System.Collections;

public class ResumeGame : MonoBehaviour {

	public void Resume()
	{
		Time.timeScale = 1.0F;
	}
}
