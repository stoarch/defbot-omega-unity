﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public void Pause()
	{
		Time.timeScale = 0.0F;
	}
}
