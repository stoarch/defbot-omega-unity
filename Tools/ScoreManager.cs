﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public int score = 0;

	public void AddScore (int val)
	{
		score += val;
	}
}
