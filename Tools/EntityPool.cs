﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EntityPool : MonoBehaviour {

	private Dictionary<String, List<GameObject>> pool = new Dictionary<String, List<GameObject>>();
	private Vector2 offScreen = new Vector2( -10, -10 );
	private Dictionary<String, GameObject> prefabDict = new Dictionary<String, GameObject>();

	public void RegisterPrefab( string tag, GameObject pref )
	{
		if( prefabDict.ContainsKey( tag ) )
			prefabDict[ tag ] = pref;
		else
			prefabDict.Add( tag, pref );
	}
		
	public bool DisableEntity( GameObject entity )
	{
		if( !pool.ContainsKey( entity.tag ) )
		{
			Debug.LogWarning( "Entity not found: " + entity.tag );
			return false;
		}

		entity.SetActive( false );
		entity.transform.position = offScreen;
		
		return true;
	} 
	
	private const int maxTagEntities = 10;
	
	public GameObject GetEntity( string tag )
	{			
		if( !pool.ContainsKey( tag ) )
		{
			InitEntityCache( tag );
		}
	
		var vals = pool[ tag ];
		
		for (int i = 0; i < vals.Count; i++) 
		{
			if( !vals[i].activeSelf	)
				return vals[i]; 
		}	
		
		return null;
	}
	
	void InitEntityCache( string tag )
	{
		var prefab = prefabDict[ tag ];
		
		var vals = new List<GameObject>();
		for (int i = 0; i < maxTagEntities; i++) 
		{
			var newPref = Instantiate( prefab ) as GameObject;
			newPref.SetActive( false );
			newPref.transform.position = offScreen;
			newPref.transform.parent = transform;
			
			vals.Add( newPref ); 	
		}
		
		pool.Add( tag, vals );			
	}
}
