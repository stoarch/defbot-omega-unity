﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreViewer : MonoBehaviour {

	public ScoreManager scoreMgr;
	public Text scoreText;
	
	private bool destroying = false;
	
	void Start()
	{
		StartCoroutine("RefreshScore");
	}
	
	void OnDestroy()
	{
		destroying = true;
		StopCoroutine( "RefreshScore" );
	}
	
	IEnumerator RefreshScore()
	{
		var wait = new WaitForSeconds( 0.3F );
		
		while( !destroying )
		{
			scoreText.text = scoreMgr.score.ToString();
			yield return wait;
		} 
	}
}
