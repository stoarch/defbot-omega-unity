﻿using UnityEngine;
using System.Collections;
using System;

public class MakeBomb : MonoBehaviour {

	public EntityPool pool;
	public String bombTag = "EnemyBomb";
	
	public void MakeIt( Vector2 pos )
	{
		var bomb = pool.GetEntity( bombTag );
		if( !bomb )
		{
			Debug.LogError( "Bomb does not exists with tag: " + bombTag );
			return;
		}
		
		bomb.transform.position = pos;
		bomb.SetActive( true );
	}
	
	public void MakeRandom()
	{
		MakeIt( new Vector2( UnityEngine.Random.Range( 0F, 1F ), 3F ) );
	}
}
