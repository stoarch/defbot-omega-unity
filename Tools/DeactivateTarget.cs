﻿using UnityEngine;
using System.Collections;

public class DeactivateTarget : MonoBehaviour {

	public GameObject target;
	
	public void Deactivate()
	{
		target.SetActive( false );
	}
}
