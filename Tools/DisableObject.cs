﻿using UnityEngine;
using System.Collections;

public class DisableObject : MonoBehaviour {

	public void Disable()
	{
		gameObject.SetActive( false );
	}
}
