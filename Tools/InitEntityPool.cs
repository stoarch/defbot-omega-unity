﻿using UnityEngine;
using System.Collections;

public class InitEntityPool : MonoBehaviour {

	public EntityPool pool;
	
	public GameObject[] prefabs;
	
	void Start()
	{
		for (int i = 0; i < prefabs.Length; i++) 
		{
			pool.RegisterPrefab( prefabs[i].tag, prefabs[i] );
		}
	}
}
