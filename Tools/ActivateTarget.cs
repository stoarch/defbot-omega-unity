﻿using UnityEngine;
using System.Collections;

public class ActivateTarget : MonoBehaviour {

	public GameObject target;
	
	public void Activate()
	{
		target.SetActive( true );
	}
}
