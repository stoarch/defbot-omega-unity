﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	public string levelName;
	
	public void Load()
	{
		Time.timeScale = 1.0F;
		Application.LoadLevel( levelName );
	}
}
