﻿using UnityEngine;
using System.Collections;

public class DisableAfterTime : MonoBehaviour {

	public float timeout = 1.0F;
	
	void Start () {
		Invoke( "Disable", timeout );	
	}
	
	void Disable()
	{
		gameObject.SetActive( false );
	}
}
