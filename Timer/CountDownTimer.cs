﻿using UnityEngine;
using System.Collections;

public class CountDownTimer : MonoBehaviour {

	public int secondsCount = 60;
	
	public int currentSeconds = 0;
	
	void Start()
	{
		currentSeconds = secondsCount;
		StartCoroutine( "StartCountdown" );
	}	
	
	IEnumerator StartCountdown()
	{
		var wait = new WaitForSeconds( 1.0F );
		while( currentSeconds > 0 )
		{
			yield return wait;
			currentSeconds--;
		}
	}
}
