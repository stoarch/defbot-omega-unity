﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CountdownTimerViewer : MonoBehaviour {

	public CountDownTimer timer;
	public Text timerText;
	
	void Start () 
	{
		StartCoroutine( "DisplayTimer" );
	}
	
	IEnumerator DisplayTimer()
	{
		var wait = new WaitForSeconds( 0.5F );
		
		while( true )
		{
			int minutes = timer.currentSeconds / 60;
			int seconds = timer.currentSeconds - minutes*60;
			
			timerText.text = String.Format("{0}:{1}", minutes, seconds);
			yield return wait;
		}
	}
	
}
