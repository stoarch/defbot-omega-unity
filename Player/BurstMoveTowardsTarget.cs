﻿using UnityEngine;
using System.Collections;

public delegate void AfterMove();

public class BurstMoveTowardsTarget : MonoBehaviour {

	public float secondsToMove = 1.0F;
	public float rotSpeed = 300.0F;//angles
	public float nearEnough = 1.0F;
	
	private Vector2 destCoord;
	private AfterMove endMove;
	private bool endMoveOnce = false;
	
	public void OnEndMoveOnce( AfterMove onAfterMove )
	{
		endMove = onAfterMove;
		endMoveOnce = true;
	}	
	
	public void MoveTo( Vector2 dest )
	{
		destCoord = dest;

		StopCoroutine( "BurstMove" );		
		StartCoroutine( "BurstMove" );		

		StopCoroutine( "RotateTowardTarget" );
		StartCoroutine( "RotateTowardTarget" );
	}
	
	IEnumerator RotateTowardTarget()
	{
		var wait = new WaitForSeconds( 0.01F );

		var destAngle = Vector2.Angle( Vector2.right, ( (Vector2)transform.position - destCoord ) );
		
		if( transform.position.y < destCoord.y )
			destAngle = 360 - destAngle;
			
		destAngle += 180;
		
		while( true )
		{
			var angle = Mathf.MoveTowardsAngle( transform.eulerAngles.z, destAngle, rotSpeed*Time.deltaTime );
			
			if( angle == destAngle )
				break;
			
			var newAngle = transform.eulerAngles;
			newAngle.z = angle;
			transform.eulerAngles = newAngle;
		
			yield return wait;	
		}		
	}
	
	IEnumerator BurstMove()
	{
		var wait = new WaitForSeconds( 0.01F );
		var lastTime = Time.time + secondsToMove;
		var dist = ( (Vector2)transform.position - destCoord ).magnitude;
				
		while( Time.time < lastTime )
		{
			var newCoord = new Vector2( 
               Mathf.MoveTowards( transform.position.x, destCoord.x, dist*Time.deltaTime ),
               Mathf.MoveTowards( transform.position.y, destCoord.y, dist*Time.deltaTime ));
			
			transform.position = newCoord;	
			
			var dist1 = ( (Vector2)transform.position - destCoord ).magnitude;
			if( dist1 <= nearEnough )
				break;			
			
			yield return wait;
		}
		
		CallOnEndMove();	
	}
	
	void CallOnEndMove()
	{		
		if( !endMoveOnce )
			return;
			
		if( endMove == null ) 
			return;
			
		endMoveOnce = false;
		endMove();		
	}
}
