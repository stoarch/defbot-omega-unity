﻿using UnityEngine;
using System.Collections;

public class MoveToMouse : MonoBehaviour {

	public BoxCollider2D bounds;
	public BurstMoveTowardsTarget mover;
	
	void Update () 
	{
		if( Input.GetMouseButtonUp(0) )
		{
			var coord = Camera.main.ScreenToWorldPoint( Input.mousePosition );
			
			if( bounds.OverlapPoint( coord ) )
			{
				mover.nearEnough = 1.0F;
				
				mover.MoveTo( coord );
			}
		}	
	}
	
}
