﻿using UnityEngine;
using System.Collections;

public class EntitySensor : MonoBehaviour {

	public string entityTag;
	
	//for debugging [HideInInspector]
	public GameObject entity;
	public bool entityDetected = false;

	public bool HasDetected ()
	{
		return entityDetected;
	}
	
	public void Clear()
	{
		entity = null;
		entityDetected = false;
	}
	
	void OnTriggerEnter2D( Collider2D other )
	{
		if( other.gameObject.CompareTag( entityTag ) )
		{
			entityDetected = true;
			entity = other.gameObject;
		}
	}
	
	void OnTriggerExit2D( Collider2D other )
	{
		if( other.gameObject.CompareTag( entityTag ) )
		{
			entity = null;
			entityDetected = false;
		}
	}
}
