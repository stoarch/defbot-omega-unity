﻿using UnityEngine;
using System.Collections;

public class MoveUp : MonoBehaviour {

	public float speed = 0.1F;
	
	void Update () 
	{
		var pos = transform.position;
		pos.y += speed;
		transform.position = pos;
	}
}
