﻿using UnityEngine;
using System.Collections;

public class EntityLifeMeter : MonoBehaviour {

	public int curValue = 3;
	public int maxValue = 3;

	public void Decrease (int val)
	{
		curValue -= val;
	}
}
