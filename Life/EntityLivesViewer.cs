﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EntityLivesViewer : MonoBehaviour {

	public EntityLifeMeter entityLife;
	public Text lifeText;
	public Text lastLiveText;
		
	private int oldLife = 0;
	
	void Start()
	{
		oldLife = entityLife.curValue;
	}
	
	void Update () 
	{
		if( oldLife == entityLife.curValue )
			return;
			
		oldLife = entityLife.curValue;
		
		if( oldLife == 1 )
			if( lastLiveText )
				lastLiveText.gameObject.SetActive( true );
		
		lifeText.text = oldLife.ToString();
	}
}
