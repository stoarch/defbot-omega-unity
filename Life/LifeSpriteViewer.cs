﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class LifeSpriteViewer : MonoBehaviour {

	public LifeMeter life;
	public Sprite[] lifeSprites;
	
	private int oldLife;
	private SpriteRenderer spriteRenderer;
	void Start()
	{
		oldLife = life.currentLife;
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	void Update()
	{
		if( oldLife == life.currentLife )
			return;
			
		oldLife = life.currentLife;
		spriteRenderer.sprite = lifeSprites[ oldLife - 1 ];
	}
}
