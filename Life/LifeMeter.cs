﻿using UnityEngine;
using System.Collections;
using System;

public class LifeMeter : MonoBehaviour {

	public int currentLife = 3;
	public int maxLife = 3;
	public bool destroyOnZeroLife = true;
	public EntityLifeMeter entityMeter;
	
	public bool invulnerableOnStart = false;
	public float invulnerableTime = 0.5F;
	public bool isInvulnerable { get; private set; }
	public bool invulnerableOnHit = false;
	
	public Animator entityAnimator;
	public String invulnerableAnimFlag;
	
	public bool reinitOnZeroLife = false;
	public bool reinitPos = false;
	public Transform startPos;
	
	public bool explosionOnDeath = false;
	public EntityPool pool;
	public string explosionTag;
	
	public AudioClip explosionAudio;
	public AudioSource audioSource;

	float oldInvulnerableTime;

	void StartInvulnerableMode ()
	{
		isInvulnerable = true;
		oldInvulnerableTime = Time.time;
		if (entityAnimator)
			entityAnimator.SetBool (invulnerableAnimFlag, true);
	}

	void Start()
	{
		if( invulnerableOnStart )
		{
			StartInvulnerableMode ();
			
		}
		
		if( !audioSource )
			audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	void Update()
	{
		if( isInvulnerable )
		{
			if( oldInvulnerableTime + invulnerableTime < Time.time )
			{
				isInvulnerable = false;
				
				if( entityAnimator )
					entityAnimator.SetBool( invulnerableAnimFlag, false );
			}
		}
	}

	public void Hit (int damage)
	{
		if( isInvulnerable )
			return;
			
		currentLife -= damage;
		
		if( invulnerableOnHit )
		{
			StartInvulnerableMode();
		}
		
		if( currentLife <= 0 )
		{
			if( explosionOnDeath )
			{
				var explosion = pool.GetEntity( explosionTag );
				explosion.transform.position = transform.position;
				explosion.SetActive( true );
				
				if( audioSource & explosionAudio )
					audioSource.PlayOneShot( explosionAudio );	
			}
			
			if( entityMeter )
				entityMeter.Decrease(1);
			
			if( destroyOnZeroLife )
				DestroyObject();
			else if( reinitOnZeroLife )
			{
				if( entityMeter.curValue > 0 )
					ReinitLife();
				else
					gameObject.SetActive( false );//no more objects to init
			}
		}
	}
	
	void ReinitLife()
	{
		currentLife = maxLife;
		
		if( reinitPos )
			transform.position = startPos.position;
		
		if( invulnerableOnStart )
			StartInvulnerableMode();	
	}
	
	void DestroyObject()
	{
		Destroy( gameObject );
	}
}
