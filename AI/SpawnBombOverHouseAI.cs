﻿using UnityEngine;
using System.Collections;
using System;

public class SpawnBombOverHouseAI : MonoBehaviour {

	public MakeBomb bombMaker;
	public EntitySensor sensor;
	public float spawnInterval = 1.0F;
	public float spawnProbability = 0.2F;
	public EntityLifeMeter lastHouses;
	public float spawnDelta = 0.1F;
	
	public AudioClip fireAudio;
	public AudioSource fireAudioSource;
	
	private float oldSpawnTime = 0F;
	private float maxSpawnInterval;
	
	void Start()
	{
		maxSpawnInterval = spawnInterval;
		
		fireAudioSource = gameObject.GetComponent<AudioSource>();
		 
	}
	
	void Update () 
	{
		if( sensor.HasDetected() )
		{				
			if( Time.time - oldSpawnTime < spawnInterval )
				return;
				
			var val = UnityEngine.Random.value;
			if( val > spawnProbability )
				return;
				
			oldSpawnTime = Time.time;
			bombMaker.MakeIt( transform.position );
			
			if( fireAudioSource )
				fireAudioSource.PlayOneShot( fireAudio );
			
			spawnInterval = maxSpawnInterval - (lastHouses.maxValue - lastHouses.curValue)*spawnDelta;
		}
	}
}
