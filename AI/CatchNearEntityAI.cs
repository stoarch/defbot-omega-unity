﻿using UnityEngine;
using System.Collections;

public class CatchNearEntityAI : MonoBehaviour {
	
	public string entityTag;
	public EntitySensor sensor;
	public BurstMoveTowardsTarget mover;
	public EntityPool pool;
	public Animator catchAnimator;
	public string catchTrigger;
	public BoxCollider2D operationBounds;
	public ScoreManager scoreMgr;
	
	public float chewTime = 0.5F;
	public string chewParam;
	public Animator chewAnimator;
	
	public AudioClip chewAudio;
	public AudioSource audioSource;
	
	private bool chewing = false;
	private float chewStartTime;
	
	void Start()
	{
		if( !sensor )
			return;
			
		sensor.entityTag = entityTag;
		
		if( !audioSource )
			audioSource = gameObject.GetComponent<AudioSource>();
	}

	void Update () 
	{
		if( chewStartTime + chewTime < Time.time )
		{
			chewing = false;
			chewAnimator.SetBool( chewParam, false );
		}
			
		if( chewing )
			return;
			
		if( sensor.HasDetected() )
		{
			if( catchAnimator )
			{	
				catchAnimator.SetTrigger( catchTrigger );
			}

			mover.nearEnough = 0.3F;
			mover.OnEndMoveOnce( ReturnToOperationBounds );
			mover.MoveTo( sensor.entity.transform.position );
			
			pool.DisableEntity( sensor.entity );
			sensor.Clear();			
			
			scoreMgr.AddScore( 100 );
			
			chewing = true;
			chewStartTime = Time.time;
			chewAnimator.SetBool( chewParam, true );
			
			if( chewAudio && audioSource )
				audioSource.PlayOneShot( chewAudio );
		}
	}
	
	void ReturnToOperationBounds()
	{
		if( operationBounds.OverlapPoint( transform.position ) )
			return;
			
		var pos = transform.position;
		pos.y = operationBounds.bounds.center.y;
		
		mover.MoveTo( pos );
	}
}
