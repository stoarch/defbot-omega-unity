﻿using UnityEngine;
using System.Collections;
using System;

public class AllyAI : MonoBehaviour {

	public EntitySensor shootEnemySensor;
	public EntitySensor rightEnemySensor;
	public EntitySensor leftEnemySensor;
	
	public EntitySensor leftEmptySpaceSensor;
	public EntitySensor rightEmptySpaceSensor;
	public EntitySensor enemyProjectileSensor;
	
	public float deltaX = 1.0F;
	public float shootTimeout = 1.0F;
	
	public float minMove = 1.0F; 
	public float maxMove = 5.0F;
	
	public float minX = -5.0F;
	public float maxX = 5.0F;
	
	public AudioClip fireAudio;
	public AudioSource fireAudioSource;
	
	private float lastShootTime = 0F;
	
	
	void Start()
	{
		StartCoroutine( "RandomMovement" );
	}
	
	public EntityPool pool;
	public String laserTag = "LaserShot";
	
	public void MakeLaserShot( Vector2 pos )
	{
		var laserShot = pool.GetEntity( laserTag );
		if( !laserShot )
		{
			Debug.LogError( "Laser shot does not exists with tag: " + laserTag );
			return;
		}
		
		laserShot.transform.position = pos;
		laserShot.SetActive( true );
		
		if( fireAudioSource )
			fireAudioSource.PlayOneShot( fireAudio );	
	}
	
	IEnumerator RandomMovement()
	{
		while(true)
		{
			var secondsToMove = UnityEngine.Random.Range( 0.5F, 2F );
			
			var destCoord = transform.position;
			
			if( UnityEngine.Random.value > 0.5F )//to right
			{
				destCoord.x += UnityEngine.Random.Range( minMove, maxMove );
			}
			else //to left
			{
				destCoord.x -= UnityEngine.Random.Range( minMove, maxMove );
			}
					
			if( destCoord.x > maxX )
				destCoord.x = maxX;
			if( destCoord.x < minX )
				destCoord.x = minX;
			
			var wait = new WaitForSeconds( 0.01F );
			var lastTime = Time.time + secondsToMove;
			var dist = ( transform.position - destCoord ).magnitude;
			
			
			while( Time.time < lastTime )
			{
				var newCoord = new Vector2( 
				                           Mathf.MoveTowards( transform.position.x, destCoord.x, dist*Time.deltaTime ),
				                           Mathf.MoveTowards( transform.position.y, destCoord.y, dist*Time.deltaTime ));
				
				transform.position = newCoord;	
						
				yield return wait;
			}
		}	
	}
	
	void Update () 
	{
		if( shootEnemySensor.HasDetected() )
		{
			if( Time.time - lastShootTime < shootTimeout )
				return;
				
			lastShootTime = Time.time;
			
			MakeLaserShot( transform.position );
		}	
	}
}
