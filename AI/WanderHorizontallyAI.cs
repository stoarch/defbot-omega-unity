﻿using UnityEngine;
using System.Collections;

public class WanderHorizontallyAI : MonoBehaviour {

	public Transform startPoint;
	public Transform endPoint;
	public float travelTime = 3.0F;
	
	private bool destroying = false;
	
	void Start()
	{
		StartTraveling();
	}	
	
	void OnDestroy()
	{
		destroying = true;
		StopCoroutine( "Travel" );
	}
	
	void StartTraveling()
	{
		StopCoroutine("Travel");
		StartCoroutine("Travel");
	}
	
	IEnumerator Travel()
	{
		var wait = new WaitForSeconds(0.01F);
		var point = endPoint.position;
		var speed = (endPoint.position - startPoint.position).magnitude/travelTime;
		var forward = true;
		
		while( !destroying )
		{
			var newX = Mathf.MoveTowards( transform.position.x, point.x, speed*Time.deltaTime );
			var pos = transform.position;
			
			pos.x = newX;
			transform.position = pos;
			
			yield return wait;
			
			if( Mathf.Abs( newX  - point.x ) < 0.1F )
			{
				if( forward )
				{
					point = startPoint.position;
					forward = false;
				}
				else
				{
					point = endPoint.position;
					forward = true;
				}
			}
		}
	}
}
