﻿using UnityEngine;
using System.Collections;
using System;

public class FireLaserOnEntity : MonoBehaviour {

	public EntitySensor sensor;
	public float spawnInterval = 1.0F;
	public float spawnProbability = 0.01F;
	
	private float oldSpawnTime = 0F;
	
	
	public EntityPool pool;
	public String laserTag = "EnemyLaserShoot";
	
	public AudioClip fireAudio;
	public AudioSource fireAudioSource;
	
	void Start()
	{
		fireAudioSource = gameObject.GetComponent<AudioSource>();
	}
		
	public void MakeLaserShot( Vector2 pos )
	{
		if( !pool )
		{
			Debug.Log( "No pool detected " + this );
			return;
		}
		
		var laserShot = pool.GetEntity( laserTag );
		if( !laserShot )
		{
			Debug.LogError( "Laser shot does not exists with tag: " + laserTag );
			return;
		}
		
		laserShot.transform.position = pos;
		laserShot.SetActive( true );
		
		if( fireAudioSource )
			fireAudioSource.PlayOneShot( fireAudio );
	}
	
	
	void Update () 
	{
		if( sensor.HasDetected() )
		{				
			if( Time.time - oldSpawnTime < spawnInterval )
				return;			
	
			if( UnityEngine.Random.value < spawnProbability )
				return;
		
			oldSpawnTime = Time.time;
			MakeLaserShot( transform.position );
		}
	}
}
