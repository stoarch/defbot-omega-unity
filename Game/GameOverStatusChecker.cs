﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverStatusChecker : MonoBehaviour {

	public EntityLifeMeter houseLives;
	public EntityLifeMeter playerLives;
	public Text gameOverText;
	
	void Update () 
	{
		
		if(( playerLives.curValue <= 0 ))
		{
			gameOverText.gameObject.SetActive(true);
			Time.timeScale = 0F;
		}
		
		if(( houseLives.curValue <= 0 ))
		{
			gameOverText.gameObject.SetActive(true);
			Time.timeScale = 0F;
		}
	}
}
