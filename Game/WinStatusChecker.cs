﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinStatusChecker : MonoBehaviour {

	public CountDownTimer resqueTimer;
	public EntityLifeMeter enemyLives;
	public Text winText;
	
	void Update () 
	{
		if(( resqueTimer.currentSeconds <= 0 )||( enemyLives.curValue <= 0 ))
		{
			winText.gameObject.SetActive(true);
			Time.timeScale = 0F;		
		}
	}
}
