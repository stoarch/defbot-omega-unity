﻿using UnityEngine;
using System.Collections;

public class GameScene : MonoBehaviour {

	public AudioSource audioSource;
	
	private static AudioSource privateAudioSource;
	
	public static void PlayOnce (AudioClip audio)
	{
		if( privateAudioSource )
			privateAudioSource.PlayOneShot( audio );
	}

	void Start () {
		privateAudioSource = audioSource;
	}
	
}
